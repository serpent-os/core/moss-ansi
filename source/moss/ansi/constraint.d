/*
 * This file is part of moss-ansi.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.ansi.constraint;

/**
 * Simple box type for constraint definition
 */
struct Constraint
{
    /**
     * Width of a component
     */
    int width = 0;

    /**
     * Height of a component
     */
    int height = 0;

    /**
     * X position for a component
     */
    int x = 1;

    /**
     * Y position for a component
     */
    int y = 1;

    /**
     * Return true if one constraint intersects another
     */
    pure bool intersect(ref Constraint other) @safe @nogc nothrow
    {
        return ((other.x >= this.x && other.y >= this.y)
                && (other.x <= this.x + this.width && other.y <= this.y + this.height));
    }
}
