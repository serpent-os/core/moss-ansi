/*
 * This file is part of moss-ansi.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.ansi.bin;

public import moss.ansi.container;
import std.exception : enforce;

/**
 * The Bin can handle a single Element and has zero layout options.
 */
public class Bin : Container
{
    /**
     * Add a child element and own it
     */
    override final void add(Element element)
    {
        enforce(childElement is null, "Bin.add(): Cannot add child as we already have one");
        childElement = element;
        childElement.parent = this;

        /* clamp allocatedConstraints */
        //childElement.allocatedConstraints = Constraint(1, 1, allocatedConstraints.width, allocatedConstraints.height);
        childElement.allocatedConstraints = Constraint(0, 0, 0, 0);
        invalidate();
    }

    /**
     * Remove a child element if we have it
     */
    override final void remove(Element element)
    {
        enforce(childElement == element, "Bin.remove(): Cannot remove child we don't own");
        childElement.parent = null;
        childElement = null;
        invalidate();
    }

    /**
     * Handle drawing of a single child
     */
    override final void draw()
    {
        if (childElement is null)
        {
            return;
        }

        /* Not invalidated. */
        if (!(invalidated || childElement.invalidated))
        {
            return;
        }

        Console.clear(transformedConstraint());

        childElement.invalidated = true;
        childElement.draw();
        childElement.invalidated = false;
        invalidated = false;
    }

private:

    Element childElement;
}
