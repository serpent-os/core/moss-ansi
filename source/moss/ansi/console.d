/*
 * This file is part of moss-ansi.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.ansi.console;

public import moss.ansi.constraint;
public import moss.ansi.style;

import std.stdio : stdout;

/**
 * Simple API to control the console.
 */
public struct Console
{
    /**
     * Render the given text at the constraint given, stripping any styling
     */
    static void render(in Constraint origin, in string renderable)
    {
        moveCursor(origin);
        stdout.writef("%s%s%s", cast(string) TextStyle.Reset, renderable,
                cast(string) TextStyle.Reset);
        stdout.flush();
    }

    /**
     * Clear the entire screen, moving the cursor to the home position
     */
    static void clear()
    {
        resetCursor();
        stdout.write("\033[J");
        stdout.flush();
    }

    /**
     * Clear the entire constraint
     */
    static void clear(in Constraint origin)
    {
        moveCursor(origin);
        foreach (row; 0 .. origin.height)
        {
            foreach (column; 0 .. origin.width)
            {
                stdout.write(" ");
            }
        }
        stdout.flush();
    }

    /**
     * Hide the cursor
     */
    static void hideCursor()
    {
        stdout.write("\033[?25l");
    }

    /**
     * show the cursor
     */
    static void showCursor()
    {
        stdout.write("\033[?25h");
    }

    /**
     * Reset cursor to home position
     */
    static void resetCursor()
    {
        stdout.write("\033[H");
    }

private:

    /**
     * Move the cursor to the start of the constraint
     */
    static void moveCursor(in Constraint origin)
    {
        stdout.writef("\033[%d;%dH", origin.y, origin.x);
    }
}
