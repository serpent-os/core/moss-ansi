/*
 * This file is part of moss-ansi.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.ansi.progressbar;

public import moss.ansi.element;
import moss.ansi.style;
import std.math : floor;

/**
 * Character for a full segment
 */
const auto segmentFull = "⬜";

/**
 * Character for an empty segment
 */
const auto segmentEmpty = "⬛";

/**
 * Partially complete
 */
const auto segmentPartial = "🔲";

/**
 * Required number of segments
 */
const auto numSegments = 10;

/**
 * A Progressbar style widget
 */
public class ProgressBar : Element
{
    /**
     * Construct a new ProgressBar
     */
    this()
    {
        allocatedConstraints.width = numSegments;
        update();
    }

    override void draw()
    {
        Console.render(transformedConstraint(), pbar);
    }

    /**
     * Return the min value
     */
    pragma(inline, true) pure @property double minValue() @safe @nogc nothrow
    {
        return _minValue;
    }

    /**
     * Return the max value
     */
    pragma(inline, true) pure @property double maxValue() @safe @nogc nothrow
    {
        return _maxValue;
    }

    /**
     * Return the current value
     */
    pragma(inline, true) pure @property double currentValue() @safe @nogc nothrow
    {
        return _currentValue;
    }

    /**
     * Set the minimum value
     */
    @property void minValue(double minValue) @safe
    {
        if (_minValue == minValue)
        {
            return;
        }
        _minValue = minValue;
        update();
    }

    /**
     * Set the max value
     */
    @property void maxValue(double maxValue) @safe
    {
        if (_maxValue == maxValue)
        {
            return;
        }
        _maxValue = maxValue;
        update();
    }

    /**
     * Set the current value
     */
    @property void currentValue(double currentValue) @safe
    {
        if (_currentValue == currentValue)
        {
            return;
        }
        _currentValue = currentValue;
        update();
    }

private:

    /**
     * Update our display status for the next render.
     */
    void update() @safe
    {
        assert(minValue < maxValue);
        assert(currentValue >= minValue && currentValue <= maxValue);

        renderFraction = (currentValue - minValue) / (maxValue - minValue);

        const double renderableSegments = renderFraction * cast(double) numSegments;
        int emptySegments = numSegments - (cast(int) renderableSegments);

        pbar = "";
        const auto flooredSegments = floor(renderableSegments);
        foreach (i; 0 .. flooredSegments)
        {
            pbar ~= segmentFull;
        }

        foreach (i; renderableSegments .. numSegments)
        {
            if (i == renderableSegments && flooredSegments < renderableSegments)
            {
                pbar ~= segmentPartial;
            }
            else
            {
                pbar ~= segmentEmpty;
            }
        }
        invalidate();
    }

    double _minValue = 0.0;
    double _maxValue = 1.0;
    double _currentValue = 0.0;

    double renderFraction = 0.0;
    string pbar = "";
}
