/*
 * This file is part of moss-ansi.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.ansi.box;

public import moss.ansi.container;

import std.algorithm : each, filter;

/**
 * Box layout is defined at creation time.
 */
public enum BoxOrientation
{
    /**
     * Box should be laid out horizontally
     */
    Horizontal,

    /**
     * Box should be laid out vertically
     */
    Vertical,
}

/**
 * A Box can layout components vertically or horizontally.
 */
public class Box : Container
{

    /**
     * Construct new layout
     */
    this(BoxOrientation orient = BoxOrientation.Vertical)
    {
        _orient = orient;
        allocatedConstraints = Constraint(0, 0, 0, 0);
    }

    override final void add(Element element)
    {
        elements ~= element;
        element.parent = this;
        applyLayout();
    }

    override final void remove(Element element)
    {
        import std.algorithm : remove;

        elements = elements.remove!((e) => element == e);
        applyLayout();
    }

    override final void draw()
    {
        auto nestedChildren = elements.filter!((e) => e.invalidated || this.invalidated);

        /* All of us are invalidated */
        if (invalidated)
        {
            Console.clear(transformedConstraint());
            elements.each!((e) => {
                e.invalidated = true;
                e.draw();
                e.invalidated = false;
            }());
            invalidated = false;
            return;
        }

        /* Only *some* elements are invalidated */
        nestedChildren.each!((e) => {
            e.invalidated = true;
            Console.clear(e.transformedConstraint());
            e.draw();
            e.invalidated = false;
        }());

        invalidated = false;
    }

private:

    void applyLayout()
    {
        int startX = transformedConstraint.x;
        int startY = transformedConstraint.y;

        allocatedConstraints.width = 1;
        allocatedConstraints.height = 1;

        /* Mutate all constraint X,Y for layout */
        elements.each!((ref e) => {
            e.allocatedConstraints.x = startX;
            e.allocatedConstraints.y = startY;

            final switch (_orient)
            {
            case BoxOrientation.Vertical:
                startY += e.allocatedConstraints.height;
                allocatedConstraints.height += e.allocatedConstraints.height;
                break;
            case BoxOrientation.Horizontal:
                startX += e.allocatedConstraints.width;
                allocatedConstraints.width += e.allocatedConstraints.width;
                break;
            }
        }());
        invalidate();
    }

    Element[] elements;
    BoxOrientation _orient = BoxOrientation.Vertical;
}
