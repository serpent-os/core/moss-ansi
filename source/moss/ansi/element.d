/*
 * This file is part of moss-ansi.
 *
 * Copyright © 2020-2021 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

module moss.ansi.element;

public import moss.ansi.console;
public import moss.ansi.constraint;

/**
 * TUI elements must implement the Element interface to properly box themselves
 * prior to rendering.
 */
public abstract class Element
{

    /**
     * Return the element allocatedConstraints
     */
    pragma(inline, true) pure ref @property Constraint allocatedConstraints() @safe @nogc nothrow
    {
        return _allocatedConstraints;
    }

    /**
     * Update allocatedConstraints and return them
     */
    pragma(inline, true) pure ref @property Constraint allocatedConstraints(Constraint newConstraint) @safe @nogc nothrow
    {
        _allocatedConstraints = newConstraint;
        return _allocatedConstraints;
    }

    /**
     * Implementations should begin their drawing routine here
     */
    abstract void draw();

    /**
     * Return true if the rendering state has been invalidated
     */
    pragma(inline, true) pure @property bool invalidated() @safe @nogc nothrow
    {
        return _invalidated;
    }

    /**
     * Forcibly invalidate this element's rendering state
     */
    pure void invalidate() @safe @nogc nothrow
    {
        invalidated = true;
    }

    /**
     * Return the parent element
     */
    pragma(inline, true) pure @property Element parent() @safe @nogc nothrow
    {
        return _parent;
    }

    /**
     * Return a constraint transformed to parents
     */
    pure @property Constraint transformedConstraint() @safe @nogc nothrow
    {
        Constraint baseConstraint = allocatedConstraints();
        Element parentElement = parent();

        while (parentElement !is null)
        {
            baseConstraint.x += parentElement.allocatedConstraints.x;
            baseConstraint.y += parentElement.allocatedConstraints.y;
            parentElement = parentElement.parent();
        }

        return baseConstraint;
    }

package:

    /**
     * Update the invalidated property
     */
    pragma(inline, true) pure @property void invalidated(bool b) @safe @nogc nothrow
    {
        _invalidated = b;
        if (b && parent !is null)
        {
            parent.invalidate();
        }
    }

    pragma(inline, true) pure @property void parent(Element parent) @safe @nogc nothrow
    {
        _parent = parent;
    }

private:

    Constraint _allocatedConstraints = Constraint.init;
    bool _invalidated = true;
    Element _parent = null;
}
